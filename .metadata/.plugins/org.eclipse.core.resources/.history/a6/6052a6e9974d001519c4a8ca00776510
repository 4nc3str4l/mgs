package com.muriology.server;

import game.Game;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public final class Server{
    
    private final int port, startingBet;
    private String deck;
    private boolean running;
    private ServerSocket server;
    private Socket socket;

    public Server(int port, int startingBet, String deck){
        this.port = port;
        this.startingBet = startingBet;
        this.deck = deck;
        try {
            server = new ServerSocket(port);
            this.start();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }    
    }

    public void start(){
        this.running = true;
        System.out.println("Server listening at port " + this.port + "...");
        while (running) {
            try {
                socket = server.accept();
            } catch (IOException e) {
                    System.out.println(e.getMessage());
            }
            //Create a new Game
            Thread game = new Thread(new Game(socket, this.startingBet, this.deck));
            game.start();
            System.out.println("Waiting for a new client");
        }
    }

    public void stop(){
            this.running = false;
    }

    public static void main(String[]args){
    //initialize the desired variables
      int port;
      int startingBet;
      String deck;

      if (args.length == 0) {
        System.out.println("You must provide execution arguments please use java Server -h for help.");
      }else if(args.length == 1 && args[0].equals("-h")){
        System.out.println("Us: java Server -p <port> -b <startingBet> -f <deckfile>");
      }else if(args.length == 6){
        //creck errors
        if(!args[0].equals("-p")){
          System.out.println("Incorrect execution format: Expected \"-p\" as first argument");
          return;
        }

        if(!args[2].equals("-b")){
          System.out.println("Incorrect execution format: Expected \"-b\" as 3rth argument");
          return;
        }
        if(!args[4].equals("-f")){
          System.out.println("Incorrect execution format: Expected \"-f\" as 5rth argument");
          return;
        }

        try{
          port = Integer.parseInt(args[1]);

        }catch(NumberFormatException nfe){
          System.out.println("Incorrect execution format: expected and interger number as a port");
          return;
        }

        try{
          startingBet = Integer.parseInt(args[3]);

        }catch(NumberFormatException nfe){
          System.out.println("Incorrect execution format: expected and interger number as a starting bet");
          return;
        }
        deck = args[5];

        //Initialize the server
        new Server(port, startingBet, deck);

      }else{
        System.out.println("Incorrect execution format: please use java Server -h for help");
      }
    }
}